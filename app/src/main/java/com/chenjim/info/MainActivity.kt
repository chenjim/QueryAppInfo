package com.chenjim.info

import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pkg = "com.chenjim.app2"
        val info = try {
            packageManager.getPackageInfo(pkg, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            null
        }
        Log.d(TAG, "$pkg info ${info?.versionName}")

        val bt = findViewById<Button>(R.id.go_other_bt)
        if (info == null) {
            bt.isEnabled = false
        } else {
            bt.setOnClickListener {
                val cName = ComponentName(pkg, "com.chenjim.app2.App2Activity")
                val intent = Intent.makeMainActivity(cName)
                startActivity(intent)
            }
        }
    }
}