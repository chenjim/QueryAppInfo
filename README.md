# QueryAppInfo

#### 介绍 Android 11 获取、启动其他应用
在 [app](app/src/main/java/com/chenjim/info/MainActivity.kt) 中有如下代码
``` kotlin
    val pkg = "com.chenjim.app2"
    val info = try {
        packageManager.getPackageInfo(pkg, 0)
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        null
    }
    Log.d(TAG, "$pkg info ${info?.versionName}")

    val bt = findViewById<Button>(R.id.go_other_bt)
    if (info == null) {
        bt.isEnabled = false
    } else {
        bt.setOnClickListener {
            val cName = ComponentName(pkg, "com.chenjim.app2.App2Activity")
            val intent = Intent.makeMainActivity(cName)
            startActivity(intent)
        }
    }

```
需要在 [AndroidManifest.xml](app/src/main/AndroidManifest.xml) 中添加以下配置，才能正常获取到app2的信息并启动 [app2](app2)
```xml
<queries>
    <package android:name="com.chenjim.app2"/>
    <intent>
        <action android:name="android.intent.action.SEND" />
        <data android:mimeType="image/jpeg" />
    </intent>
</queries>
```


